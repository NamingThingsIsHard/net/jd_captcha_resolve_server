"""
jd_captcha_resolve_server
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os
import threading

from blinker import signal
from flask import Flask, render_template, url_for, request

from jd_captcha_resolve_server.receiver import MyUnixStreamServer


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    app.config["IMAGE_DIR"] = os.environ.get("IMAGE_DIR", f"{os.getcwd()}/static")

    # ensure the instance folder exists
    os.makedirs(app.instance_path, exist_ok=True)

    socket = MyUnixStreamServer(
        # TODO: put socket into config
        "/tmp/killer.sock",
        app,
        bind_and_activate=True
    )

    def start_socket(*args):
        socket.serve_forever()
        print("served")

    def stop_socket(*args):
        socket.server_close()

    # Shutdown the socket server when the app stops
    # TODO make sure this isn't called after each request
    app.teardown_appcontext(stop_socket)

    threading.Thread(target=start_socket).start()

    @app.route("/")
    def hello():
        return "Hello, World!"

    @app.route("/captcha/<captcha_id>")
    def captcha(captcha_id):
        # TODO check if the file exists
        return render_template(
            "captcha.html.jinja",
            captcha_id=captcha_id,
            captcha_url=url_for("static", filename=f"{captcha_id}.jpg"),
            post_url=url_for("captcha_resolve", captcha_id=captcha_id)
        )

    @app.route("/captcha/<captcha_id>/resolve", methods=["POST"])
    def captcha_resolve(captcha_id):
        # print(request.form)
        # return redirect(url_for("hello"))
        captcha_signal = signal(f'captcha-{captcha_id}')
        captcha_signal.send("captcha_resolve", resolution=request.form['captcha'])
        return render_template(
            "captcha_resolved.html.jinja",
            captcha=request.form['captcha'],
        )

    return app
