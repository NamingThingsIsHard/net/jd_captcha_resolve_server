"""
jd_captcha_resolve_server
Copyright (C) 2020 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import hashlib
import logging
import os
import threading
from collections import defaultdict, namedtuple
from pathlib import Path
from socket import socket
from socketserver import StreamRequestHandler, ThreadingUnixStreamServer

from blinker import signal
from flask import url_for


class MyUnixStreamServer(ThreadingUnixStreamServer):
    class RequestHandler(StreamRequestHandler):
        """
        @ivar server: MyUnixStreamServer
        @var server: MyUnixStreamServer
        @type server: MyUnixStreamServer
        """

        def handle(self):
            #TODO handle exceptions
            app = self.server.app
            hex_digest = self._save_file()

            captcha_signal = signal(f'captcha-{hex_digest}')
            event = threading.Event()
            collection = defaultdict(str)

            @captcha_signal.receiver_connected.connect
            def on_receiver_connected(*args, **kw):
                # Send event to listeners so that they can unblock
                # captcha_signal.send(self, event=event)
                app.logger.info(f"Receiver connected: {args} kwargs: {kw}")

            @captcha_signal.connect
            def on_connected(send, **kw):
                collection["resolution"] = kw["resolution"]
                event.set()

            # Send notification
            # app.logger.info(f"Go to http://localhost/{url_for('captcha_resolve', captcha_id=hex_digest)}")
            # TODO build URL properly here
            app.logger.info(f"Go to http://localhost:5000/captcha/{hex_digest}")

            # Wait for user to solve it
            event.wait(self.server.response_timeout)

            # TODO get rid of image file

            # send back user's response
            self.request.sendall(bytes(collection["resolution"].encode()))

        def _save_file(self):
            app = self.server.app  # type: flask.app.Flask

            # TODO: calc image dir from static path
            image_dir = Path(app.config["IMAGE_DIR"])
            request = self.request  # type: socket
            cum_data = bytes()
            data = request.recv(1024)
            while len(data):
                cum_data += data
                data = request.recv(1024)
                if not data or not len(data):
                    print(data)
            # cum_data = request.makefile().read(-1)
            hasher = self.server.hasher
            hasher.update(cum_data)
            hex_digest = hasher.hexdigest()
            image_path = image_dir / f"{hex_digest}.jpg"
            image_path.write_bytes(cum_data)
            return hex_digest

    def __init__(self, socket_path, app, response_timeout=60, bind_and_activate=False):
        """
        
        @param socket_path: 
        @type socket_path: basestring
        @param app: 
        @type app: flask.app.Flask
        """
        self.app = app
        self.hasher = hashlib.sha256()  # type: hashlib.sha3_256
        self.response_timeout = response_timeout

        path = Path(socket_path)
        if path.exists():
            path.unlink()
        super().__init__(
            socket_path,
            self.RequestHandler,
            bind_and_activate=bind_and_activate
        )


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    App = namedtuple("App", ["config", "logger"])
    sock = "/tmp/nothing.sock"
    if os.path.exists(sock):
        os.remove(sock)
    with MyUnixStreamServer(
            sock,
            App(
                config={
                    "IMAGE_DIR": "/tmp/somewhere"
                },
                logger=logging.getLogger("something")
            ),
            bind_and_activate=True
    ) as s:
        s.serve_forever()
    os.remove(sock)
