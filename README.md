This is a minimal [Flask][Flask] server for [JDownloader][JDownloader] to send captchas to
 that need resolving.

# Background

JDownloader is a downloader manager that can handle downloading from many different sources.
Some of those sources require the user to resolve captchas and JDownloader does allow a user
 to resolve them if they are currently sitting in front of the screen.
When a user is AFK though, they miss the resolution request and the file is skipped.

JDownloader luckily does support [external methods of captcha resolution][jd-extern-captcha].

# How this works

This server can be spun up, wait for JDownloader to send images to resolve, notify a user remotely
 that a captcha is in need of resolving and respond to JDownload once the user provices a resolution.

Communication between JD and server is done via a UNIX socket that accepts the raw captcha image.
JD then waits for response on the same connection.

Notifications are done using [Signal][Signal] over [signal-cli][signal-cli].
These contain a link to the captcha to resolve that will present the user with a web-form.

The user resolves the captcha by simply entering the solution into an input field.
The result is sent to the server that in turn communicates it back to JDownloader.

# Setup

TODO

[JDownloader]: https://jdownloader.org
[jd-extern-captcha]: https://jdownloader.org/knowledge/wiki/development/captcha/extern-captcha-method
[Signal]: https://signal.org
[signal-cli]: https://github.com/AsamK/signal-cli
